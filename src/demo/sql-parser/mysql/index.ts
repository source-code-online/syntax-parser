import { createParser } from '../../..';
import { IStatements } from '../base/define';
// 分词器
import { sqlTokenizer } from './lexer';
import { root } from './parser';

/**
 * mysql解析器
 * @export
 * @type {*}
 */
export const mysqlParser = createParser<IStatements>(root, sqlTokenizer, {
  cursorTokenExcludes: token => {
    return token.value === '.' || token.value === ':';
  },
});
