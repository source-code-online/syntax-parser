import { mysqlParser } from '../sql-parser';

// eslint-disable-next-line no-restricted-globals
const ctx: Worker = self as any;

ctx.onmessage = event => {
  // 没有用到 parserType
  // 现在只有mysql解析器
  // args：{SQL语句, 游标位置}
  ctx.postMessage(mysqlParser(event.data.text, event.data.index));
};

export default null as any;
