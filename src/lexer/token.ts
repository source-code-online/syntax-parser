export enum TokenType {
  SPECIAL = 'special',
  WORD = 'word',
  STRING = 'string',
  CURSOR = 'cursor',
}

/**
 * Token接口，切割后的SQL
 * @see https://github.com/dt-fe/weekly/blob/master/64.精读《手写%20SQL%20编译器%20-%20词法分析》.md
 * @export
 * @interface IToken
 * @property type: string 类型
 * @property value: string 值
 * @property position?: [number, number]
 */
export interface IToken {
  /**
   * 类型：注释、关键字-word（select、create...）、操作符-operator（+、-、>=）、
   * 开闭合标志（(、case）、占位符（?）、空格、引号包含的文本/数子/字段、方言语法（${variable}）
   *
   * @type {string}
   * @memberof IToken
   */
  // TODO type使用TokenType
  type: string;
  value: string;
  /**
   * token在SQL语句的定位
   *
   * @type {[number, number]}
   * @memberof IToken
   */
  position?: [number, number];
}
