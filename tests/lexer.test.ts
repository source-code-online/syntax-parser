import { createLexer } from '../src/lexer';

const myLexer = createLexer([
  {
    type: 'whitespace',
    regexes: [/^(\s+)/],
    ignore: true,
  },
  {
    type: 'word',
    regexes: [/^([a-zA-Z0-9]+)/],
  },
  {
    type: 'operator',
    regexes: [/^(\+)/],
  },
]);

const sqlLexer = createLexer([
  {
    type: 'whitespace',
    regexes: [/^(\s+)/],
    ignore: true,
  },
  {
    type: 'comment',
    regexes: [
      /^((?:#|--).*?(?:\n|$))/, // # --
      /^(\/\*[^]*?(?:\*\/|$))/, // /* */
    ],
    ignore: true,
  },
  {
    type: 'number',
    regexes: [/^([0-9]+(\.[0-9]+)?|0x[0-9a-fA-F]+|0b[01]+)\b/],
  },
  {
    type: 'word',
    regexes: [
      /^([a-zA-Z0-9_]+)/, // word
      /^(\$\{[a-zA-Z0-9_]+\})/, // ${word}
    ],
  },
  {
    type: 'string',
    regexes: [
      /^((?=")(?:"[^"\\]*(?:\\[\s\S][^"\\]*)*"))/, // ""
      /^((?=')(?:'[^'\\]*(?:\\[\s\S][^'\\]*)*'))/, // ''
      /^((?=`)(?:`[^`\\]*(?:\\[\s\S][^`\\]*)*`))/, // ``
    ],
  },
  {
    type: 'special',
    regexes: [
      /^(\(|\))/, // '(' ')'.
      /^(!=|<>|==|<=|>=|!<|!>|\|\||::|->>|->|~~\*|~~|!~~\*|!~~|~\*|!~\*|!~|.)/, // operators.
    ],
  },
]);

test('lexer-demo', () => {
  console.log(myLexer('ab + c'));
});

test('sql-lexer', () => {
  console.log(sqlLexer(`select * from user u where u.name = '小明'`));
});
