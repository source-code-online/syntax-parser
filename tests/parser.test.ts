import { chain, createLexer, createParser, many, matchTokenType } from '../src';

const myLexer = createLexer([
  {
    type: 'whitespace',
    regexes: [/^(\s+)/],
    ignore: true,
  },
  {
    type: 'word',
    regexes: [/^([a-zA-Z0-9]+)/],
  },
  {
    type: 'operator',
    regexes: [/^(\+)/],
  },
]);

const sqlLexer = createLexer([
  {
    type: 'whitespace',
    regexes: [/^(\s+)/],
    ignore: true,
  },
  {
    type: 'comment',
    regexes: [
      /^((?:#|--).*?(?:\n|$))/, // # --
      /^(\/\*[^]*?(?:\*\/|$))/, // /* */
    ],
    ignore: true,
  },
  {
    type: 'number',
    regexes: [/^([0-9]+(\.[0-9]+)?|0x[0-9a-fA-F]+|0b[01]+)\b/],
  },
  {
    type: 'word',
    regexes: [
      /^([a-zA-Z0-9_]+)/, // word
      /^(\$\{[a-zA-Z0-9_]+\})/, // ${word}
    ],
  },
  {
    type: 'string',
    regexes: [
      /^((?=")(?:"[^"\\]*(?:\\[\s\S][^"\\]*)*"))/, // ""
      /^((?=')(?:'[^'\\]*(?:\\[\s\S][^'\\]*)*'))/, // ''
      /^((?=`)(?:`[^`\\]*(?:\\[\s\S][^`\\]*)*`))/, // ``
    ],
  },
  {
    type: 'special',
    regexes: [
      /^(\(|\))/, // '(' ')'.
      /^(!=|<>|==|<=|>=|!<|!>|\|\||::|->>|->|~~\*|~~|!~~\*|!~~|~\*|!~\*|!~|.)/, // operators.
    ],
  },
]);

const root = () => {
  // eslint-disable-next-line no-use-before-define
  return chain(addExpr)(ast => {
    return ast[0];
  });
};

const addExpr = () => {
  // eslint-disable-next-line no-use-before-define
  return chain(matchTokenType('word'), many(addPlus))(ast => {
    return {
      left: ast[0].value,
      operator: ast[1] && ast[1][0].operator,
      right: ast[1] && ast[1][0].term,
    };
  });
};

const addPlus = () => {
  return chain(['+', '-'], root)(ast => {
    return {
      operator: ast[0].value,
      term: ast[1],
    };
  });
};

const myParser = createParser(
  root, // Root grammar.
  myLexer, // Created in lexer example.
);

test('parser demo', () => {
  // eslint-disable-next-line no-console
  console.log(myParser('a + b'));
});

test('work demo', () => {
  // 实例化一个 worker
  // const worker: Worker = new (MyWorker as any)();
});
